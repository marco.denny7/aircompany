using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AirCompanyApi.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Surname { get; set; }

        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Password { get; set; }

        [JsonIgnore]
        public virtual List<Role> Roles { get; set; } = new();
    }
}