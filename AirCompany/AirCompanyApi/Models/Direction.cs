using System;
using System.ComponentModel.DataAnnotations;

namespace AirCompanyApi.Models
{
    public class Direction
    {
        public int DirectionId { get; set; }
        
        [Required]
        public string DepartureCity { get; set; }
        
        [Required]
        public DateTime DepartureTime { get; set; }
        
        [Required]
        public string ArrivalCity { get; set; }

        public DateTime ArrivalTime { get; set; }

        public int QuantityPax { get; set; } = 0;

        [Required]
        public virtual Flight Flight { get; set; }
    }
}