using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AirCompanyApi.Models
{
    public class Flight
    {
        public int FlightId { get; set; }

        public string Customer { get; set; }

        public decimal Price { get; set; }

        public string Comment { get; set; }
        
        [Required]
        public virtual List<Direction> Directions { get; set; }

        [Required]
        public virtual List<Employee> Crew { get; set; }

        [Required]
        public virtual Aircraft Aircraft { get; set; }
    }
}