using System.ComponentModel.DataAnnotations;

namespace AirCompanyApi.Models
{
    public class Aircraft
    {
        public int AircraftId { get; set; }

        [Required] public string Name { get; set; }

        [Required] public string BoardNumber { get; set; }

        public bool IsFree { get; set; } = true;
    }
}