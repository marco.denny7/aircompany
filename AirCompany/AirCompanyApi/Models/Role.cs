using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AirCompanyApi.Models
{
    public class Role
    {
        public int RoleId { get; set; }

        [Required]
        [Remote(action: "Create", controller: "Roles", AdditionalFields = nameof(Name))]
        public string Name { get; set; }

        public bool SeeFlightPrice { get; set; }

        public bool SeeFlightAirCraft { get; set; }

        public bool SeeFlightCustomer { get; set; }

        public bool SeeFlightQuantityPax { get; set; }

        public bool SeeFlightDirections { get; set; }

        public bool SeeFlightComment { get; set; }
        
        public bool SeeCrew { get; set; }
        
        public bool CanManageEmployee { get; set; }
        
        public bool CanManageRole { get; set; }
        
        public bool CanManageAircraft { get; set; }

        public bool CanManageFlight { get; set; }
        
        [JsonIgnore]
        public virtual List<Employee> Employees { get; set; } = new();
    }
}