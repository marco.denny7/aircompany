using AirCompanyApi.DataBase;
using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Management;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Repository;
using AirCompanyApi.Repository.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace AirCompanyApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.PreserveReferencesHandling =
                    (PreserveReferencesHandling) ReferenceLoopHandling.Ignore);

            services.AddDbContext<AirCompanyContext>(options =>
                options.UseLazyLoadingProxies().UseNpgsql(Configuration["ConnectionStrings:DefaultConnection"]));

            services.AddSwaggerGen();
            services.AddTransient<IAirCompanyContext, AirCompanyContext>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IAircraftRepository, AircraftRepository>();
            services.AddTransient<IEmployeeManage, EmployeeManage>();
            services.AddTransient<IRoleManage, RoleManage>();
            services.AddTransient<IAircraftManage, AircraftManage>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();

            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Air Company API"); });
        }
    }
}