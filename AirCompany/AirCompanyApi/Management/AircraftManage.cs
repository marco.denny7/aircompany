using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Management
{
    public class AircraftManage : IAircraftManage
    {
        private readonly IAircraftRepository _aircrafts;
        private readonly IRoleRepository _roles;

        public AircraftManage(IAircraftRepository aircrafts, IRoleRepository roles)
        {
            _aircrafts = aircrafts;
            _roles = roles;
        }

        public List<Aircraft> GetAll()
        {
            return _aircrafts.GetAll();
        }

        public Aircraft GetById(int id)
        {
            return _aircrafts.GetById(id);
        }

        public bool RoleCanManageAircraft(int id)
        {
            var role = _roles.GetById(id);

            if (role is null || role.CanManageAircraft is false)
            {
                return false;
            }

            return true;
        }

        public void Create(out bool isAdded, ref string message, Aircraft aircraft)
        {
            isAdded = CheckAircraft(aircraft, ref message);

            if (AircraftExists(aircraft))
            {
                message = $"Самолёт с бортовым номером \"{aircraft.BoardNumber}\" уже есть!";
                isAdded = false;
            }

            if (isAdded)
            {
                _aircrafts.Add(aircraft);
            }
        }

        public void Update(Aircraft aircraft, ref bool isUpdated, ref string message)
        {
            isUpdated = CheckAircraft(aircraft, ref message);

            if (AircraftWithSameBoardNumberExists(aircraft))
            {
                message = $"Самолёт с бортовым номером \"{aircraft.BoardNumber}\" уже есть!";
                isUpdated = false;
            }

            if (isUpdated)
            {
                _aircrafts.Update(aircraft);
            }
        }

        public void Delete(Aircraft aircraft)
        {
            _aircrafts.Delete(aircraft);
        }

        public void Save()
        {
            _aircrafts.Save();
        }


        private bool CheckAircraft(Aircraft aircraft, ref string message)
        {
            if (string.IsNullOrWhiteSpace(aircraft.Name) || string.IsNullOrEmpty(aircraft.Name))
            {
                message = "Не передано название самолёта!";
                return false;
            }

            if (string.IsNullOrWhiteSpace(aircraft.BoardNumber) || string.IsNullOrEmpty(aircraft.BoardNumber))
            {
                message = "Не передан бортовой номер самолёта!";
                return false;
            }

            return true;
        }

        private bool AircraftExists(Aircraft aircraft)
        {
            return _aircrafts.GetAll().Any(a => a.BoardNumber == aircraft.BoardNumber);
        }

        private bool AircraftWithSameBoardNumberExists(Aircraft aircraft)
        {
            return _aircrafts.GetAll().Any(a =>
                a.BoardNumber == aircraft.BoardNumber && a.AircraftId != aircraft.AircraftId);
        }
    }
}