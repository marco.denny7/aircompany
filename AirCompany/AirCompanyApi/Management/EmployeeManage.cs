using System;
using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Management
{
    public class EmployeeManage : IDisposable, IEmployeeManage
    {
        private readonly List<Employee> _newEmployees = new();
        private readonly List<Employee> _dirtyEmployees = new();
        private readonly List<Employee> _removedEmployees = new();
        private readonly List<Employee> _storedEmployees = new();

        private readonly IAirCompanyContext _context;
        private readonly IEmployeeRepository _employees;
        private readonly IRoleRepository _roles;
        private bool disposed;

        public EmployeeManage(IEmployeeRepository employees, IRoleRepository roles)
        {
            _employees = employees;
            _roles = roles;
        }

        public Employee GetById(int id)
        {
            var employee = _storedEmployees.FirstOrDefault(e => e.EmployeeId == id);
            if (employee is null)
            {
                employee = _employees.GetById(id);
                AddToStored(employee);
            }

            return employee;
        }

        public List<Employee> GetAll()
        {
            List<Employee> employees = null;
            try
            {
                employees = _employees.GetAll();
                AddToStored(employees);
            }
            catch (Exception e)
            {
                // TODO запись в логи
                Console.WriteLine(e);
            }

            return employees;
        }

        public void RegisterNew(Employee employee, ref bool isAdded, ref string message)
        {
            AddEmployee(out isAdded, ref message, employee);
        }

        public void RegisterDirty(Employee employee, ref bool isUpdated, ref string message)
        {
            if (employee.EmployeeId != 0
                && !_newEmployees.Contains(employee)
                && !_dirtyEmployees.Contains(employee))
            {
                _dirtyEmployees.Add(employee);
                isUpdated = true;
            }
        }

        public void RegisterDelete(Employee employee)
        {
            bool removedFromNewAccount = _newEmployees.Remove(employee);
            if (removedFromNewAccount) return;
            _dirtyEmployees.Remove(employee);
            if (employee.EmployeeId != 0 && !_removedEmployees.Contains(employee))
            {
                _removedEmployees.Add(employee);
            }
        }

        public void Commit()
        {
            InsertNew();
            DeleteRemoved();
            UpdateDirty();
        }

        public void Disposing(Employee employee)
        {
            if (employee != null && _removedEmployees.Contains(employee))
            {
                _removedEmployees.Remove(employee);
            }
        }

        public Role GetRole(int id)
        {
            return _roles.GetById(id);
        }

        public void SetRoleForEmployee(Employee employee, Role role, ref bool isAdded, ref string message)
        {
            AddRoleForEmployee(employee, role, out isAdded, ref message);
            if (!isAdded) return;
            _employees.Save();
            UpdateStoredEmployees(employee);
        }

        public void RemoveEmployeeRole(Employee employee, Role role, ref bool removed, ref string message)
        {
            RemoveRoleFromEmployee(employee, role, out removed, ref message);
            if (!removed) return;
            _employees.Save();
            UpdateStoredEmployees(employee);
        }

        public bool CanManageEmployee(int id)
        {
            var role = _roles.GetById(id);

            if (role is null || role.CanManageEmployee is false)
            {
                return false;
            }

            return true;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        private void InsertNew()
        {
            if (!IsNullOrEmpty(_newEmployees))
            {
                foreach (var employee in _newEmployees)
                {
                    _employees.Add(employee);
                    AddToStored(employee);
                }

                _newEmployees.Clear();
                _employees.Save();
            }
        }

        private void AddToCollectionNew(Employee employee)
        {
            var isContainsNew = _newEmployees.Contains(employee);
            var isContainsDirty = _dirtyEmployees.Contains(employee);

            if (employee != null && !isContainsNew && !isContainsDirty)
            {
                _newEmployees.Add(employee);
            }
        }

        private bool CheckEmployee(Employee employee, ref string message)
        {
            if (string.IsNullOrEmpty(employee.Name))
            {
                message = "Не передано имя сотрудника!";
                return false;
            }

            if (string.IsNullOrEmpty(employee.Surname))
            {
                message = "Не передана фамилия сотрудника!";
                return false;
            }

            if (string.IsNullOrEmpty(employee.Email))
            {
                message = "Не передан Email сотрудника!";
                return false;
            }

            if (string.IsNullOrEmpty(employee.Password))
            {
                message = "Не передан пароль сотрудника!";
                return false;
            }

            if (EmployeeWitEmailExists(employee.Email))
            {
                message = "Сотрудник с таким Email уже существует!";
                return false;
            }

            if (EmployeeWithInitialsExists(employee))
            {
                message = "Такой сотрудник уже существует!";
                return false;
            }

            return true;
        }

        private void AddToStored(Employee employee)
        {
            var isContainsStored = _storedEmployees.Contains(employee);
            var isContainsDirty = _dirtyEmployees.Contains(employee);
            var isContainsRemoved = _removedEmployees.Contains(employee);

            if (employee != null && !isContainsStored && !isContainsDirty && !isContainsRemoved)
            {
                _storedEmployees.Add(employee);
            }
        }

        private void AddToStored(List<Employee> employees)
        {
            foreach (var employee in employees)
            {
                AddToStored(employee);
            }
        }

        private void DeleteRemoved()
        {
            if (!IsNullOrEmpty(_removedEmployees))
            {
                foreach (var employee in _removedEmployees)
                {
                    _employees.Delete(employee);
                    RemoveFromStored(employee);
                }

                _removedEmployees.Clear();
                _employees.Save();
            }
        }

        private void RemoveFromStored(Employee employee)
        {
            if (!IsNullOrEmpty(_storedEmployees))
            {
                var elementForRemove =
                    _storedEmployees.FirstOrDefault(e => e.EmployeeId == employee.EmployeeId);

                if (elementForRemove != null)
                {
                    _storedEmployees.Remove(elementForRemove);
                }
            }
        }

        private void UpdateDirty()
        {
            if (!IsNullOrEmpty(_dirtyEmployees))
            {
                foreach (var employee in _dirtyEmployees)
                {
                    _employees.Update(employee);
                    UpdateStoredEmployees(employee);
                }

                _dirtyEmployees.Clear();
                _employees.Save();
            }
        }

        private void UpdateStoredEmployees(Employee employee)
        {
            var storedEmployee =
                _storedEmployees.FirstOrDefault(e => e.EmployeeId == employee.EmployeeId);

            if (storedEmployee == null) return;

            storedEmployee.Name = employee.Name;
            storedEmployee.Surname = employee.Surname;
            storedEmployee.Email = employee.Email;
            storedEmployee.Password = employee.Password;
            storedEmployee.Roles = employee.Roles;
        }

        private void AddEmployee(out bool isAdded, ref string message, Employee employee)
        {
            isAdded = CheckEmployee(employee, ref message);
            if (isAdded)
            {
                AddToCollectionNew(employee);
            }
        }

        private bool EmployeePresentRole(Employee employee, Role role)
        {
            return employee.Roles.Any(r => r.RoleId == role.RoleId);
        }

        private void AddRoleForEmployee(Employee employee, Role role, out bool added, ref string message)
        {
            var employeeHasRole = EmployeePresentRole(employee, role);
            if (!employeeHasRole)
            {
                employee.Roles.Add(role);
                message =
                    $"Сотрудник: \"{employee.Name} {employee.Surname}\" теперь занимает должность \"{role.Name}\"";
                added = true;
            }
            else
            {
                message = $"Сотрудник: \"{employee.Name} {employee.Surname}\" уже занимает должность \"{role.Name}\"";
                added = false;
            }
        }

        private void RemoveRoleFromEmployee(Employee employee, Role role, out bool removed, ref string message)
        {
            var employeeHasRole = EmployeePresentRole(employee, role);
            if (employeeHasRole)
            {
                _employees.RemoveEmployeeRole(employee, role);
                message =
                    $"Сотрудник: \"{employee.Name} {employee.Surname}\" больше не занимает должность \"{role.Name}\"";
                removed = true;
            }
            else
            {
                message =
                    $"Сотрудник: \"{employee.Name} {employee.Surname}\" не занимает должность \"{role.Name}\"";
                removed = false;
            }
        }

        private bool IsNullOrEmpty(List<Employee> collection)
        {
            return collection is null && collection.Count == 0;
        }
        
        private bool EmployeeWithInitialsExists(Employee employee)
        {
            var name = employee.Name;
            var secondName = employee.Surname;
            var email = employee.Email;
            return _employees.GetAll().Any(e => e.Name == name && e.Surname == secondName && e.Email == email);
        }
        
        private bool EmployeeWitEmailExists(string email)
        {
            return _employees.GetAll().Any(e => e.Email == email);
        }
    }
}