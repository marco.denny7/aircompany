using System.Collections.Generic;
using AirCompanyApi.Models;

namespace AirCompanyApi.Management.Interfaces
{
    public interface IAircraftManage
    {
        List<Aircraft> GetAll();
        Aircraft GetById(int id);
        bool RoleCanManageAircraft(int id);
        void Create(out bool isAdded, ref string message, Aircraft aircraft);
        void Update(Aircraft aircraft, ref bool isUpdated, ref string message);
        void Delete(Aircraft aircraft);
        void Save();
        // Role GetRole(int id);
    }
}