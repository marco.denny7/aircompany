using AirCompanyApi.Models;

namespace AirCompanyApi.Management.Interfaces
{
    public interface IEmployeeManage : IManage<Employee>
    {
        Role GetRole(int id);

        void SetRoleForEmployee(Employee employee, Role role, ref bool isAdded, ref string message);

        void RemoveEmployeeRole(Employee employee, Role role, ref bool removed, ref string message);
        
        bool CanManageEmployee(int id);
    }
}