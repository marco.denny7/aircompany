using AirCompanyApi.Models;

namespace AirCompanyApi.Management.Interfaces
{
    public interface IRoleManage : IManage<Role>
    {
        Employee GetEmployee(int employeeId);

        void SetEmployeeForRole(Employee employee, Role role, ref bool isAdded, ref string message);

        void RemoveRoleEmployee(Role role, Employee employee, ref bool removed, ref string message);

        bool CanManageRole(int id);
    }
}