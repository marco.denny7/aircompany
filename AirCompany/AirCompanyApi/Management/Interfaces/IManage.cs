using System.Collections.Generic;

namespace AirCompanyApi.Management.Interfaces
{
    public interface IManage<Model>
    {
        Model GetById(int id);

        List<Model> GetAll();

        void RegisterNew(Model entity, ref bool isAdded, ref string message);

        void RegisterDirty(Model entity, ref bool isUpdated, ref string message);

        void RegisterDelete(Model entity);

        void Commit();

        void Disposing(Model entity);
    }
}