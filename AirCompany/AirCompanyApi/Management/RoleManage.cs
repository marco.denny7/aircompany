using System;
using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Management
{
    public class RoleManage : IDisposable, IRoleManage
    {
        private readonly List<Role> _newRoles = new();
        private readonly List<Role> _dirtyRoles = new();
        private readonly List<Role> _removedRoles = new();
        private readonly List<Role> _storedRoles = new();

        private readonly IRoleRepository _roles;
        private readonly IEmployeeRepository _employees;
        private readonly IAirCompanyContext _context;
        private bool disposed;

        public RoleManage(IRoleRepository roles, IEmployeeRepository employees)
        {
            _roles = roles;
            _employees = employees;
        }

        public Role GetById(int id)
        {
            var role = _storedRoles.FirstOrDefault(r => r.RoleId == id);
            if (role is null)
            {
                role = _roles.GetById(id);
                AddToStored(role);
            }

            return role;
        }

        public List<Role> GetAll()
        {
            List<Role> roles = null;
            try
            {
                roles = _roles.GetAll();
                AddToStored(roles);
            }
            catch (Exception e)
            {
                // TODO запись в логи
                Console.WriteLine(e);
            }

            return roles;
        }

        public void RegisterNew(Role role, ref bool isAdded, ref string message)
        {
            AddRole(out isAdded, ref message, role);
        }

        public void RegisterDirty(Role role, ref bool isUpdated, ref string message)
        {
            isUpdated = CheckRole(role, ref message);
            if (isUpdated)
            {
                AddToCollectionDirty(role);
            }
        }

        public void RegisterDelete(Role role)
        {
            bool removedFromNewAccount = _newRoles.Remove(role);
            if (removedFromNewAccount) return;
            _dirtyRoles.Remove(role);
            if (role.RoleId != 0 && !_removedRoles.Contains(role))
            {
                _removedRoles.Add(role);
            }
        }

        public void Commit()
        {
            InsertNew();
            DeleteRemoved();
            UpdateDirty();
        }

        public void Disposing(Role role)
        {
            if (role != null && _removedRoles.Contains(role))
            {
                _removedRoles.Remove(role);
            }
        }

        public Employee GetEmployee(int employeeId)
        {
            return _employees.GetById(employeeId);
        }

        public void SetEmployeeForRole(Employee employee, Role role, ref bool added, ref string message)
        {
            AddEmployeeForRole(employee, role, out added, ref message);
            if (!added) return;
            _roles.Save();
            UpdateStoredRoles(role);
        }

        public void RemoveRoleEmployee(Role role, Employee employee, ref bool removed, ref string message)
        {
            RemoveEmployeeFromRole(role, employee, out removed, ref message);
            if (!removed) return;
            _roles.Save();
            UpdateStoredRoles(role);
        }
        
        public bool CanManageRole(int id)
        {
            var role = _roles.GetById(id);

            if (role is null || role.CanManageRole is false)
            {
                return false;
            }

            return true;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void AddRole(out bool isAdded, ref string message, Role role)
        {
            isAdded = CheckRole(role, ref message);
            if (isAdded)
            {
                AddToCollectionNew(role);
            }
        }

        private void AddToStored(Role role)
        {
            var isContainsStored = _storedRoles.Contains(role);
            var isContainsDirty = _dirtyRoles.Contains(role);
            var isContainsRemoved = _removedRoles.Contains(role);

            if (role != null && !isContainsStored && !isContainsDirty && !isContainsRemoved)
            {
                _storedRoles.Add(role);
            }
        }

        private void AddToStored(List<Role> roles)
        {
            foreach (var role in roles)
            {
                AddToStored(role);
            }
        }

        private void AddToCollectionNew(Role role)
        {
            var isContainsNew = _newRoles.Contains(role);
            var isContainsDirty = _dirtyRoles.Contains(role);

            if (role != null && !isContainsNew && !isContainsDirty)
            {
                _newRoles.Add(role);
            }
        }

        private void InsertNew()
        {
            if (!IsNullOrEmpty(_newRoles))
            {
                foreach (var role in _newRoles)
                {
                    _roles.Add(role);
                    AddToStored(role);
                }

                _newRoles.Clear();
                _roles.Save();
            }
        }

        private bool CheckRole(Role role, ref string message)
        {
            if (string.IsNullOrEmpty(role.Name))
            {
                message = "Не передано название должности!";
                return false;
            }
            
            if (RoleWithNameExists(role.Name))
            {
                message = "Должность с таким названием уже существует!";
                return false;
            }

            message = $"Должность {role.Name} создана";
            return true;
        }

        private void AddToCollectionDirty(Role role)
        {
            if (role.RoleId != 0 && !_newRoles.Contains(role) && !_dirtyRoles.Contains(role))
            {
                _dirtyRoles.Add(role);
            }
        }

        private void UpdateDirty()
        {
            if (!IsNullOrEmpty(_dirtyRoles))
            {
                foreach (var role in _dirtyRoles)
                {
                    _roles.Update(role);
                    UpdateStoredRoles(_dirtyRoles);
                }

                _dirtyRoles.Clear();
                _roles.Save();
            }
        }

        private void UpdateStoredRoles(Role role)
        {
            var storedRole =
                _storedRoles.FirstOrDefault(r => r.RoleId == role.RoleId);

            if (storedRole == null) return;

            storedRole.Name = role.Name;
            storedRole.SeeFlightCustomer = role.SeeFlightCustomer;
            storedRole.SeeFlightAirCraft = role.SeeFlightAirCraft;
            storedRole.SeeFlightPrice = role.SeeFlightPrice;
            storedRole.SeeFlightQuantityPax = role.SeeFlightQuantityPax;
            storedRole.SeeFlightDirections = role.SeeFlightDirections;
            storedRole.SeeFlightComment = role.SeeFlightComment;
            storedRole.SeeCrew = role.SeeCrew;
            storedRole.Employees = role.Employees;
        }

        private void UpdateStoredRoles(List<Role> roles)
        {
            if (IsNullOrEmpty(_storedRoles) || IsNullOrEmpty(roles)) return;
            foreach (var role in roles)
            {
                UpdateStoredRoles(role);
            }
        }

        private void DeleteRemoved()
        {
            if (!IsNullOrEmpty(_removedRoles))
            {
                foreach (var role in _removedRoles)
                {
                    _roles.Delete(role);
                    RemoveFromStored(role);
                }

                _removedRoles.Clear();
                _roles.Save();
            }
        }

        private void RemoveFromStored(Role role)
        {
            if (!IsNullOrEmpty(_storedRoles))
            {
                var elementForRemove =
                    _storedRoles.FirstOrDefault(r => r.RoleId == role.RoleId);

                if (elementForRemove != null)
                {
                    _storedRoles.Remove(elementForRemove);
                }
            }
        }

        private bool RolePresentEmployee(Role role, Employee employee)
        {
            return role.Employees.Any(e => e.EmployeeId == employee.EmployeeId);
        }

        private void AddEmployeeForRole(Employee employee, Role role, out bool added, ref string message)
        {
            var roleHasEmployee = RolePresentEmployee(role, employee);
            if (!roleHasEmployee)
            {
                // employee.Roles.Add(role);
                role.Employees.Add(employee);
                message =
                    $"Сотрудник: \"{employee.Name} {employee.Surname}\" теперь занимает должность \"{role.Name}\"";
                added = true;
            }
            else
            {
                message = $"Сотрудник: \"{employee.Name} {employee.Surname}\" уже занимает должность \"{role.Name}\"";
                added = false;
            }
        }

        private void RemoveEmployeeFromRole(Role role, Employee employee, out bool removed, ref string message)
        {
            var roleHasEmployee = RolePresentEmployee(role, employee);
            if (roleHasEmployee)
            {
                _roles.RemoveRoleEmployee(role, employee);
                message =
                    $"Сотрудник: \"{employee.Name} {employee.Surname}\" больше не занимает должность \"{role.Name}\"";
                removed = true;
            }
            else
            {
                message =
                    $"Сотрудник: \"{employee.Name} {employee.Surname}\" не занимает должность \"{role.Name}\"";
                removed = false;
            }
        }

        private static bool IsNullOrEmpty(List<Role> collection)
        {
            return collection is null && collection.Count == 0;
        }
        
        private bool RoleWithNameExists(string roleName)
        {
            return _roles.GetAll().Any(r => r.Name == roleName);
        }
    }
}