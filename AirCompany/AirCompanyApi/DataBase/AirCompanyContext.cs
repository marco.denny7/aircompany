using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Models;
using Microsoft.EntityFrameworkCore;

namespace AirCompanyApi.DataBase
{
    public class AirCompanyContext : DbContext, IAirCompanyContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Aircraft> AirCrafts { get; set; }

        public DbSet<Flight> Flights { get; set; }

        public AirCompanyContext()
        {
        }

        public AirCompanyContext(DbContextOptions<AirCompanyContext> options) : base(options)
        {
        }

        public void Save()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    RoleId = 1,
                    Name = "Администратор",
                    CanManageRole = true,
                    CanManageEmployee = true
                }
            );

            modelBuilder.Entity<Employee>().HasData(
                new Employee
                {
                    EmployeeId = 1,
                    Name = "Василий",
                    Surname = "Пупкин",
                    Password = "123456",
                    Email = "pupkin@gmail.com"
                }
            );
        }
    }
}