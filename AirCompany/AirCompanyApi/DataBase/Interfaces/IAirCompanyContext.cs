using AirCompanyApi.Models;
using Microsoft.EntityFrameworkCore;

namespace AirCompanyApi.DataBase.Interfaces
{
    public interface IAirCompanyContext : IDbContext
    {
        DbSet<Employee> Employees { get; }
        
        DbSet<Role> Roles { get; }
        
        DbSet<Aircraft> AirCrafts { get; }

        void Save();
    }
}