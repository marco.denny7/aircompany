using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace AirCompanyApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeManage _manage;

        public EmployeesController(IEmployeeManage manage)
        {
            _manage = manage;
        }

        [HttpGet("{roleIdForCheckManageRights:int}")]
        public IActionResult GetAll(int roleIdForCheckManageRights)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на получение информации по сотрудникам!");
            }
            
            var employees = _manage.GetAll();

            if (employees != null && employees.Count > 0)
            {
                return Ok(employees);
            }

            return NotFound("Сотрудников нет!");
        }

        [HttpGet("{id:int}")]
        public IActionResult GetEmployee(int id, int roleIdForCheckManageRights)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на получение информации по сотруднику!");
            }
            
            var employee = _manage.GetById(id);

            if (employee != null)
            {
                return Ok(employee);
            }

            return NotFound("Сотрудник с таким id не найден!");
        }

        [HttpGet("{id:int}")]
        public IActionResult GetEmployeeRoles(int id, int roleIdForCheckManageRights)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на получение информации по сотруднику!");
            }
            
            var employee = _manage.GetById(id);

            if (employee != null)
            {
                if (employee.Roles is null || employee.Roles.Count == 0)
                {
                    return NotFound($"{employee.Surname} {employee.Name} не занимает должностей!");
                }

                return Ok(employee.Roles);
            }

            return NotFound("Сотрудник с таким id не найден!");
        }

        [HttpPost("{roleIdForCheckManageRights:int}")] 
        public IActionResult Create( int roleIdForCheckManageRights, [FromBody] Employee employee)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на создание сотрудника!");
            }
            
            var isAdded = false;
            var message = "";
            if (employee == null)
            {
                return BadRequest("Не передан сотрудник!");
            }

            _manage.RegisterNew(employee, ref isAdded, ref message);
            if (isAdded)
            {
                _manage.Commit();
                return Ok($"Сотрудник \"{employee.Surname} {employee.Name}\" добавлен");
            }

            return BadRequest(message);
        }

        [HttpPost("{roleIdForCheckManageRights:int}")]
        public IActionResult SetRole(int roleIdForCheckManageRights, int id, int roleId)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на добавление должности сотруднику!");
            }
            
            var isAdded = false;
            var message = "";
            var employee = _manage.GetById(id);
            var role = _manage.GetRole(roleId);
            if (employee is null)
            {
                return NotFound("Сотрудник с таким id не найден!");
            }

            if (role is null)
            {
                return NotFound("Должность с таким id не найдена!");
            }

            _manage.SetRoleForEmployee(employee, role, ref isAdded, ref message);
            if (isAdded)
            {
                return Ok(message);
            }

            return BadRequest(message);
        }

        [HttpDelete("{roleIdForCheckManageRights:int}")]
        public IActionResult Delete(int roleIdForCheckManageRights, int id)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на удаление сотрудника!");
            }
            
            var employee = _manage.GetById(id);

            if (employee != null)
            {
                try
                {
                    _manage.RegisterDelete(employee);
                    _manage.Commit();
                    return Ok($"Сотрудник \"{employee.Surname} {employee.Name}\" удалён");
                }
                catch
                {
                    _manage.Disposing(employee);
                    return BadRequest("Не удалось удалить аккаунт!");
                }
            }

            return NotFound("Сотрудник с таким id не найден!");
        }

        [HttpDelete("{roleIdForCheckManageRights:int}")]
        public IActionResult RemoveEmployeeRole(int roleIdForCheckManageRights, int id, int roleId)
        {
            if (!_manage.CanManageEmployee(roleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на удаление должности у сотрудника!");
            }
            
            var removed = false;
            var message = "";
            var employee = _manage.GetById(id);
            var role = _manage.GetRole(roleId);
            if (employee is null)
            {
                return NotFound("Сотрудник с таким id не найден!");
            }

            if (role is null)
            {
                return NotFound("Должность с таким id не найдена!");
            }

            _manage.RemoveEmployeeRole(employee, role, ref removed, ref message);
            if (removed)
            {
                return Ok(message);
            }

            return BadRequest(message);
        }

        [HttpPut]
        public IActionResult Update([FromBody] Employee transmittedEmployee)
        {
            var isUpdated = false;
            var message = "Не удалось обновить данные сотрудника!";
            var foundEmployee = _manage.GetById(transmittedEmployee.EmployeeId);

            if (foundEmployee is null)
            {
                return NotFound("Сотрудник с таким id не найден!");
            }

            _manage.RegisterDirty(transmittedEmployee, ref isUpdated, ref message);
            if (isUpdated)
            {
                _manage.Commit();
                return Ok(transmittedEmployee);
            }

            return BadRequest(message);
        }
    }
}