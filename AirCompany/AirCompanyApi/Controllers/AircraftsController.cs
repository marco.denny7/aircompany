using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace AirCompanyApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AircraftsController : ControllerBase
    {
        private readonly IAircraftManage _manage;

        public AircraftsController(IAircraftManage manage)
        {
            _manage = manage;
        }

        [HttpGet("{roleId:int}")]
        public IActionResult GetAll(int roleId)
        {
            if (!_manage.RoleCanManageAircraft(roleId))
            {
                return BadRequest("Нет прав на получение информации по самолётам");
            }

            var aircrafts = _manage.GetAll();

            if (aircrafts != null && aircrafts.Count > 0)
            {
                return Ok(aircrafts);
            }

            return NotFound("Самолётов нет!");
        }

        [HttpGet("{roleId:int}")]
        public IActionResult GetAircraft(int id, int roleId)
        {
            if (!_manage.RoleCanManageAircraft(roleId))
            {
                return BadRequest("Нет прав на получение информации по самолёту");
            }

            var aircraft = _manage.GetById(id);

            if (aircraft != null)
            {
                return Ok(aircraft);
            }

            return NotFound("Самолёт с таким id не найден!");
        }

        [HttpPost("{roleId:int}")]
        public IActionResult Create([FromBody] Aircraft aircraft, int roleId)
        {
            var message = "";

            if (!_manage.RoleCanManageAircraft(roleId))
            {
                return BadRequest("Нет прав на создание самолёта");
            }

            if (aircraft == null)
            {
                return BadRequest("Не передан самолёт!");
            }

            _manage.Create(out var isAdded, ref message, aircraft);
            if (isAdded)
            {
                _manage.Save();
                return Ok($"Самолёт \"{aircraft.Name} с бортовым номером {aircraft.BoardNumber}\" создан");
            }

            return BadRequest(message);
        }

        [HttpDelete("{roleId:int}")]
        public IActionResult Delete(int id, int roleId)
        {
            if (!_manage.RoleCanManageAircraft(roleId))
            {
                return BadRequest("Нет прав на удаление самолёта");
            }

            var aircraft = _manage.GetById(id);

            if (aircraft != null)
            {
                try
                {
                    _manage.Delete(aircraft);
                    _manage.Save();
                    return Ok($"Самолёт \"{aircraft.BoardNumber}\" удалён");
                }
                catch
                {
                    return BadRequest($"Не удалось удалить самолёт \"{aircraft.BoardNumber}\"!");
                }
            }

            return NotFound("Самолёт с таким id не найден!");
        }

        [HttpPut("{roleId:int}")]
        public IActionResult Update([FromBody] Aircraft transmittedAircraft, int roleId)
        {
            var isUpdated = false;
            var message = "";
            
            if (!_manage.RoleCanManageAircraft(roleId))
            {
                return BadRequest("Нет прав на обновление данных самолёта");
            }

            var foundAircraft = _manage.GetById(transmittedAircraft.AircraftId);
            
            if (foundAircraft == null)
            {
                return NotFound("Самолёт с таким id не найден!");
            }

            _manage.Update(transmittedAircraft, ref isUpdated, ref message);

            if (!isUpdated) return BadRequest(message);
            _manage.Save();
            return Ok(transmittedAircraft);

        }
    }
}