using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace AirCompanyApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRoleManage _manage;

        public RolesController(IRoleManage manage)
        {
            _manage = manage;
        }

        [HttpGet("{RoleIdForCheckManageRights:int}")]
        public IActionResult GetAll(int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на получение информации по должностям!");
            }

            var roles = _manage.GetAll();

            if (roles != null && roles.Count > 0)
            {
                return Ok(roles);
            }

            return NotFound("Должностей нет!");
        }

        [HttpGet("{RoleIdForCheckManageRights:int}")]
        public IActionResult GetRole(int id, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на получение информации по должности!");
            }

            var employee = _manage.GetById(id);

            if (employee != null)
            {
                return Ok(employee);
            }

            return NotFound("Должность с таким id не найдена!");
        }

        [HttpGet("{id:int}")]
        public IActionResult GetRoleEmployees(int id, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на получение информации по должности!");
            }

            var role = _manage.GetById(id);

            if (role != null)
            {
                if (role.Employees is null || role.Employees.Count == 0)
                {
                    return NotFound($"Нет сотрдуников, которые занимают должность \"{role.Name}\"");
                }

                return Ok(role.Employees);
            }

            return NotFound("Должность с таким id не найдена!");
        }

        [HttpPost("{RoleIdForCheckManageRights:int}")]
        public IActionResult Create([FromBody] Role role, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на создание должности!");
            }

            var isAdded = false;
            var message = "";
            if (role == null)
            {
                return BadRequest("Должность не передана!");
            }

            _manage.RegisterNew(role, ref isAdded, ref message);
            if (isAdded)
            {
                _manage.Commit();
                return Ok($"Должность \"{role.Name}\" создана");
            }

            return BadRequest(message);
        }

        [HttpPost("{id:int}")]
        public IActionResult SetEmployee(int id, int employeeId, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на установление сотрудникам должности!");
            }

            var isAdded = false;
            var message = "";
            var role = _manage.GetById(id);
            var employee = _manage.GetEmployee(employeeId);
            if (role is null)
            {
                return NotFound("Должность с таким id не найдена!");
            }

            if (employee is null)
            {
                return NotFound("Сотрудник с таким id не найден!");
            }

            _manage.SetEmployeeForRole(employee, role, ref isAdded, ref message);
            if (isAdded)
            {
                return Ok(message);
            }

            return BadRequest(message);
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на удаление должности!");
            }

            var role = _manage.GetById(id);

            if (role != null)
            {
                try
                {
                    _manage.RegisterDelete(role);
                    _manage.Commit();
                    return Ok($"Должность \"{role.Name}\" удалена");
                }
                catch
                {
                    _manage.Disposing(role);
                    return BadRequest("Не удалось удалить должность!");
                }
            }

            return NotFound("Должность с таким id не найдена!");
        }

        [HttpDelete("{id:int}")]
        public IActionResult RemoveRoleEmployee(int id, int employeeId, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на удаление сотрудника у должности!");
            }

            var removed = false;
            var message = "";
            var role = _manage.GetById(id);
            var employee = _manage.GetEmployee(employeeId);
            if (employee is null)
            {
                return NotFound("Сотрудник с таким id не найден!");
            }

            if (role is null)
            {
                return NotFound("Должность с таким id не найдена!");
            }

            _manage.RemoveRoleEmployee(role, employee, ref removed, ref message);
            if (removed)
            {
                return Ok(message);
            }

            return BadRequest(message);
        }

        [HttpPut("{RoleIdForCheckManageRights:int}")]
        public IActionResult Update([FromBody] Role transmittedRole, int RoleIdForCheckManageRights)
        {
            if (!_manage.CanManageRole(RoleIdForCheckManageRights))
            {
                return BadRequest("Нет прав на изменение данных должности!");
            }

            var isUpdated = false;
            var message = "Не удалось обновить данные должности!";

            var foundRole = _manage.GetById(transmittedRole.RoleId);
            if (foundRole is null)
            {
                return NotFound("Должность с таким id не найдена!");
            }

            _manage.RegisterDirty(transmittedRole, ref isUpdated, ref message);

            if (isUpdated)
            {
                _manage.Commit();
                return Ok(transmittedRole);
            }

            return BadRequest(message);
        }
    }
}