using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Repository
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IAirCompanyContext _context;

        public RoleRepository(IAirCompanyContext context)
        {
            _context = context;
        }

        public Role GetById(int id)
        {
            return _context.Roles.Find(id);
        }

        public List<Role> GetAll()
        {
            return _context.Roles.ToList();
        }

        public void Delete(Role role)
        {
            _context.Roles.Remove(role);
        }

        public void Update(Role role)
        {
            var foundRole = GetById(role.RoleId);

            if (foundRole.Name is not (null and " "))
            {
                foundRole.Name = role.Name;
            }
            
            foundRole.SeeFlightPrice = role.SeeFlightPrice;
            foundRole.SeeFlightAirCraft = role.SeeFlightAirCraft;
            foundRole.SeeFlightCustomer = role.SeeFlightCustomer;
            foundRole.SeeFlightQuantityPax = role.SeeFlightQuantityPax;
            foundRole.SeeFlightDirections = role.SeeFlightDirections;
            foundRole.SeeFlightComment = role.SeeFlightComment;
            foundRole.SeeCrew = role.SeeCrew;
            foundRole.CanManageEmployee = role.CanManageEmployee;
            foundRole.CanManageRole = role.CanManageRole;
            foundRole.CanManageAircraft = role.CanManageAircraft;
            foundRole.CanManageFlight = role.CanManageFlight;
        }

        public void Add(Role role)
        {
            _context.Roles.Add(role);
        }

        public void Save()
        {
            _context.Save();
        }

        public void RemoveRoleEmployee(Role role, Employee employee)
        {
            var foundEmployee = _context.Employees.FirstOrDefault(e => e.EmployeeId == employee.EmployeeId);
            role.Employees.Remove(foundEmployee);
        }
    }
}