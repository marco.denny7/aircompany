using System.Collections.Generic;
using AirCompanyApi.Models;

namespace AirCompanyApi.Repository.Interfaces
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        void RemoveEmployeeRole(Employee employee, Role role);
    }
}