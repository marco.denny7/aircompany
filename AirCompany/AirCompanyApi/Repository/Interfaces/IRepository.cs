using System.Collections.Generic;

namespace AirCompanyApi.Repository.Interfaces
{
    public interface IRepository<Model> where Model : class
    {
        Model GetById(int id);

        List<Model> GetAll();

        public void Delete(Model entity);

        public void Update(Model entity);

        public void Add(Model entity);

        public void Save();
    }
}