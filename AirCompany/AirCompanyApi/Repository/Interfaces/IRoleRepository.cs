using AirCompanyApi.Models;

namespace AirCompanyApi.Repository.Interfaces
{
    public interface IRoleRepository : IRepository<Role>
    {
        void RemoveRoleEmployee(Role role, Employee employee);
    }
}