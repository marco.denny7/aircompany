using AirCompanyApi.Models;

namespace AirCompanyApi.Repository.Interfaces
{
    public interface IAircraftRepository : IRepository<Aircraft>
    {
    }
}