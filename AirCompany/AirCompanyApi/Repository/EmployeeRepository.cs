using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IAirCompanyContext _context;

        public EmployeeRepository(IAirCompanyContext context)
        {
            _context = context;
        }

        public Employee GetById(int id)
        {
            return _context.Employees.Find(id);
        }

        public List<Employee> GetAll()
        {
            return _context.Employees.ToList();
        }

        public void Delete(Employee employee)
        {
            _context.Employees.Remove(employee);
        }

        public void Update(Employee employee)
        {
            var foundEmployee = _context.Employees.Find(employee.EmployeeId);
            if (string.IsNullOrEmpty(employee.Name))
            {
                foundEmployee.Name = employee.Name;
            }

            if (string.IsNullOrEmpty(employee.Surname))
            {
                foundEmployee.Surname = employee.Surname;
            }

            if (string.IsNullOrEmpty(employee.Email))
            {
                foundEmployee.Email = employee.Email;
            }

            foundEmployee.Roles = employee.Roles;
        }

        public void Add(Employee employee)
        {
            _context.Employees.Add(employee);
        }

        public void Save()
        {
            _context.Save();
        }

        public void RemoveEmployeeRole(Employee employee, Role role)
        {
            var foundRole = _context.Roles.FirstOrDefault(r => r.RoleId == role.RoleId);
            employee.Roles.Remove(foundRole);
        }
    }
}