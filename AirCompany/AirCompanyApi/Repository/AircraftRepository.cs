using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.DataBase.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Repository
{
    public class AircraftRepository : IAircraftRepository
    {
        private readonly IAirCompanyContext _context;

        public AircraftRepository(IAirCompanyContext context)
        {
            _context = context;
        }

        public Aircraft GetById(int id)
        {
            return _context.AirCrafts.FirstOrDefault(a => a.AircraftId == id);
        }

        public List<Aircraft> GetAll()
        {
            return _context.AirCrafts.ToList();
        }

        public void Delete(Aircraft aircraft)
        {
            _context.AirCrafts.Remove(aircraft);
        }

        public void Update(Aircraft aircraft)
        {
            var foundAircraft = GetById(aircraft.AircraftId);

            foundAircraft.Name = aircraft.Name;
            foundAircraft.BoardNumber = aircraft.BoardNumber;
            foundAircraft.IsFree = aircraft.IsFree;
        }

        public void Add(Aircraft aircraft)
        {
            _context.AirCrafts.Add(aircraft);
        }

        public void Save()
        {
            _context.Save();
        }
    }
}