using System.Collections.Generic;
using AirCompanyApi.Models;

namespace AirCompanyApi.Tests
{
    public struct TestDataWithEmployees
    {
        public List<Employee> Employees => CreateEmployees();
        
        private static readonly Role _role200 = new Role
        {
            RoleId = 200,
            Name = "Водитель",
            SeeFlightPrice = false,
            SeeFlightAirCraft = true,
            SeeFlightCustomer = false,
            SeeFlightQuantityPax = false,
            SeeFlightDirections = true,
            SeeFlightComment = false,
            SeeCrew = true,
            CanManageEmployee = false,
            CanManageRole = false,
            CanManageAircraft = false,
            CanManageFlight = false,
            Employees = new List<Employee>
            {
                _employee200
            }
        };

        private static readonly Role _role1 = new Role
        {
            RoleId = 1,
            Name = "Менеджер",
            SeeFlightPrice = false,
            SeeFlightAirCraft = true,
            SeeFlightCustomer = false,
            SeeFlightQuantityPax = true,
            SeeFlightDirections = true,
            SeeFlightComment = true,
            SeeCrew = true,
            CanManageEmployee = false,
            CanManageRole = false,
            CanManageAircraft = false,
            CanManageFlight = false,
            
            Employees = new List<Employee>
            {
                _employee1
            }
        };
        
        private static readonly Role _role2 = new Role
        {
            RoleId = 2,
            Name = "Директор",
            SeeFlightPrice = true,
            SeeFlightAirCraft = true,
            SeeFlightCustomer = true,
            SeeFlightQuantityPax = true,
            SeeFlightDirections = true,
            SeeFlightComment = true,
            SeeCrew = true,
            CanManageEmployee = true,
            CanManageRole = true,
            CanManageAircraft = true,
            CanManageFlight = true,
            Employees = new List<Employee>
            {
                _employee200
            }
        };

        private static readonly Employee _employee1 = new Employee
        {
            EmployeeId = 1,
            Name = "Василий",
            Surname = "Пупкин",
            Password = "Pupok99",
            Email = "Pupkin@gmail.com",
            Roles = new List<Role>
            {
                _role1
            }
        };


        private static readonly Employee _employee2 = new Employee
        {
            EmployeeId = 2,
            Name = "Сергей",
            Surname = "Борщёв",
            Password = "Borsh99",
            Email = "SergiyBor@gmail.com",
            Roles = new List<Role>()
        };

        private static readonly Employee _employee3 = new Employee
        {
            EmployeeId = 3,
            Name = "Кузнецова",
            Surname = "Валерия",
            Password = "Kuznya99",
            Email = "KuznecovaVal@gmail.com",
        };

        private static readonly Employee _employee4 = new Employee
        {
            EmployeeId = 4,
            Name = "Шишкина",
            Surname = "Александра",
            Password = "Shyshka100",
            Email = "Shyshkina@gmail.com",
        };

        private static readonly Employee _employee200 = new Employee
        {
            EmployeeId = 200,
            Name = "Charley",
            Surname = "Chaplin",
            Password = "Chaplin999",
            Email = "Chaplin@gmail.com",
            Roles = new List<Role>
            {
                _role200
            }
        };

        // private static readonly Role _role1 = new Role
        // {
        //     RoleId = 1,
        //     Name = "Менеджер",
        //     IsSeePrice = false,
        //     IsSeeAirCraft = true,
        //     IsSeeCustomer = false,
        //     IsSeeQuantityPax = true,
        //     Employees = new List<Employee>()
        // };
        //
        // private static readonly Role _role2 = new Role
        // {
        //     RoleId = 2,
        //     Name = "Директор",
        //     IsSeePrice = true,
        //     IsSeeAirCraft = true,
        //     IsSeeCustomer = true,
        //     IsSeeQuantityPax = true,
        //     Employees = new List<Employee>()
        // };
        //
        // private static readonly Role _role3 = new Role
        // {
        //     RoleId = 3,
        //     Name = "Инженер",
        //     IsSeePrice = false,
        //     IsSeeAirCraft = true,
        //     IsSeeCustomer = false,
        //     IsSeeQuantityPax = false,
        //     Employees = new List<Employee>()
        // };
        //
        // private static readonly Role _role4 = new Role
        // {
        //     RoleId = 4,
        //     Name = "Бортпроводник",
        //     IsSeePrice = false,
        //     IsSeeAirCraft = true,
        //     IsSeeCustomer = true,
        //     IsSeeQuantityPax = true,
        //     Employees = new List<Employee>()
        // };


        private List<Employee> CreateEmployees()
        {
            return new List<Employee>
            {
                // new Employee
                // {
                //     EmployeeId = 1,
                //     Name = "Василий",
                //     Surname = "Пупкин",
                //     Password = "Pupok99",
                //     Email = "Pupkin@gmail.com",
                //     Roles = new List<Role>()
                // },
                // new Employee
                // {
                //     EmployeeId = 2,
                //     Name = "Сергей",
                //     Surname = "Борщёв",
                //     Password = "Borsh99",
                //     Email = "SergiyBor@gmail.com",
                //     Roles = new List<Role>
                //     {
                //         new Role
                //         {
                //             RoleId = 200,
                //             Name = "Водитель",
                //             IsSeePrice = false,
                //             IsSeeAirCraft = true,
                //             IsSeeCustomer = false,
                //             IsSeeQuantityPax = false
                //         }
                //     }
                // },
                // new Employee
                // {
                //     EmployeeId = 3,
                //     Name = "Кузнецова",
                //     Surname = "Валерия",
                //     Password = "Kuznya99",
                //     Email = "KuznecovaVal@gmail.com",
                // },
                // new Employee
                // {
                //     EmployeeId = 4,
                //     Name = "Шишкина",
                //     Surname = "Александра",
                //     Password = "Shyshka100",
                //     Email = "Shyshkina@gmail.com",
                // },
                // new Employee
                // {
                //     EmployeeId = 200,
                //     Name = "Charley",
                //     Surname = "Chaplin",
                //     Password = "Chaplin999",
                //     Email = "Chaplin@gmail.com",
                // }

                _employee1, _employee2, _employee3, _employee4, _employee200
            };
        }

        // private List<Role> CreateRoles()
        // {
        //     return new List<Role>
        //     {
        //         // new Role
        //         // {
        //         //     RoleId = 1,
        //         //     Name = "Менеджер",
        //         //     IsSeePrice = false,
        //         //     IsSeeAirCraft = true,
        //         //     IsSeeCustomer = false,
        //         //     IsSeeQuantityPax = true,
        //         //     Employees = new List<Employee>
        //         //     {
        //         //         new Employee
        //         //         {
        //         //             EmployeeId = 4,
        //         //             Name = "Шишкина",
        //         //             Surname = "Александра",
        //         //             Password = "Shyshka100",
        //         //             Email = "Shyshkina@gmail.com",
        //         //         }
        //         //     }
        //         // },
        //         // new Role
        //         // {
        //         //     RoleId = 2,
        //         //     Name = "Директор",
        //         //     IsSeePrice = true,
        //         //     IsSeeAirCraft = true,
        //         //     IsSeeCustomer = true,
        //         //     IsSeeQuantityPax = true,
        //         //     Employees = new List<Employee>()
        //         // },
        //         // new Role
        //         // {
        //         //     RoleId = 3,
        //         //     Name = "Инженер",
        //         //     IsSeePrice = false,
        //         //     IsSeeAirCraft = true,
        //         //     IsSeeCustomer = false,
        //         //     IsSeeQuantityPax = false,
        //         //     Employees = new List<Employee>()
        //         // },
        //         // new Role
        //         // {
        //         //     RoleId = 4,
        //         //     Name = "Бортпроводник",
        //         //     IsSeePrice = false,
        //         //     IsSeeAirCraft = true,
        //         //     IsSeeCustomer = true,
        //         //     IsSeeQuantityPax = true,
        //         //     Employees = new List<Employee>()
        //         // },
        //         // new Role
        //         // {
        //         //     RoleId = 200,
        //         //     Name = "Водитель",
        //         //     IsSeePrice = false,
        //         //     IsSeeAirCraft = true,
        //         //     IsSeeCustomer = false,
        //         //     IsSeeQuantityPax = false,
        //         //     Employees = new List<Employee>()
        //         // }
        //
        //         _role1, _role2, _role3, _role4, _role200
        //     };
        // }
    }
}