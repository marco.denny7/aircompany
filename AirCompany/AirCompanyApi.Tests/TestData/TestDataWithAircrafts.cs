using System.Collections.Generic;
using AirCompanyApi.Models;

namespace AirCompanyApi.Tests
{
    public struct TestDataWithAircrafts
    {
        public List<Aircraft> Aircrafts => CreateAircrafts();

        private static readonly Aircraft _embraerPhenom = new Aircraft
        {
            AircraftId = 1,
            Name = "Embraer Phenom 200",
            BoardNumber = "Em-009"
        };
        
        private static readonly Aircraft _cessnaCitation = new Aircraft
        {
            AircraftId = 2,
            Name = "Cessna Citation X",
            BoardNumber = "M-Best"
        };
        
        private static readonly Aircraft _cessnaCitation2 = new Aircraft
        {
            AircraftId = 3,
            Name = "Cessna Citation X",
            BoardNumber = "M-Moon"
        };
        
        private static readonly Aircraft _premier1A = new Aircraft
        {
            AircraftId = 4,
            Name = "BeachCraft Premier 1 A",
            BoardNumber = "Up - 1003"
        };

        private List<Aircraft> CreateAircrafts()
        {
            return new List<Aircraft>
            {
                _embraerPhenom, _cessnaCitation, _cessnaCitation2, _premier1A
            };
        }
    }
}