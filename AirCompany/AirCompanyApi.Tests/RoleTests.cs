using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.Controllers;
using AirCompanyApi.Management;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AirCompanyApi.Tests
{
    public class RoleTests
    {
        private IRoleRepository _roleRepository;
        private IEmployeeRepository _employeeRepository;
        private IRoleManage _manage;
        private RolesController _controller;
        private IActionResult _response;

        private const int ROLE_ID_THAT_CAN_MANAGE_ROLE = 2;
        private const int ROLE_ID_THAT_CANT_MANAGE_ROLE = 1;

        [SetUp]
        public void Setup()
        {
            _roleRepository = new TestRoleRepository();
            _employeeRepository = new TestEmployeeRepository();
            _manage = new RoleManage(_roleRepository, _employeeRepository);
            _controller = new RolesController(_manage);
        }

        [TearDown]
        public void TearDown()
        {
            _employeeRepository = null;
            _roleRepository = null;
            _manage = null;
            _controller = null;
            _response = null;
        }

        #region Get

        [Test]
        public void GetAll_ShouldReturn_OKResult()
        {
            _response = _controller.GetAll(ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var expectedResult = _roleRepository.GetAll();

            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(expectedResult, responseResultValue);
        }

        [Test]
        public void GetAll_RoleIdThatCantManageRole_BadResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по должностям!";
            _response = _controller.GetAll(ROLE_ID_THAT_CANT_MANAGE_ROLE);

            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void GetRole_ID_1_OKResult()
        {
            var expectedRole = _roleRepository.GetById(ROLE_ID_THAT_CAN_MANAGE_ROLE);

            _response = _controller.GetRole(ROLE_ID_THAT_CAN_MANAGE_ROLE, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(expectedRole, responseResultValue);
        }

        [Test]
        public void GetRole_Id_404_NotFoundResult()
        {
            const int ROLE_ID = 404;
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";

            _response = _controller.GetRole(ROLE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void GetRole_RoleId_404_RoleWithoutAccessRights()
        {
            const int ROLE_ID = 404;
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по должности!";

            _response = _controller.GetRole(ROLE_ID, ROLE_ID_THAT_CANT_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        #endregion

        #region GetRoleEmployees

        [Test]
        public void GetRoleEmployees_RoleId_200_OKResult()
        {
            const int ROLE_ID_FOR_GET_EMPLOYEES = 200;
            var foundRole = _manage.GetById(ROLE_ID_FOR_GET_EMPLOYEES);
            _response = _controller.GetRoleEmployees(ROLE_ID_FOR_GET_EMPLOYEES, ROLE_ID_THAT_CAN_MANAGE_ROLE);

            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(foundRole.Employees, responseResultValue);
        }

        [Test]
        public void GetRoleEmployees_RoleIdThatCantManageRole_BadRequest()
        {
            const int ROLE_ID_FOR_GET_EMPLOYEES = 200;
            const string expectedResponse = "Нет прав на получение информации по должности!";
            _response = _controller.GetRoleEmployees(ROLE_ID_FOR_GET_EMPLOYEES, ROLE_ID_THAT_CANT_MANAGE_ROLE);

            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(expectedResponse, responseResultValue);
        }

        [Test]
        public void GetRoleEmployees_RoleId_2_NotFoundObjectResult()
        {
            const int ROLE_ID = 2;
            var foundRole = _manage.GetById(ROLE_ID);
            _response = _controller.GetRoleEmployees(ROLE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var expectedResponse =
                $"Нет сотрдуников, которые занимают должность \"{foundRole.Name}\"";
            _response = _controller.GetRoleEmployees(ROLE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);

            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(expectedResponse, responseResultValue);
        }

        [Test]
        public void GetRoleEmployees_RoleId_404_NotFoundObjectResult()
        {
            const int ROLE_ID = 404;
            const string expectedResponse = "Должность с таким id не найдена!";
            _response = _controller.GetRoleEmployees(ROLE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);

            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(expectedResponse, responseResultValue);
        }

        #endregion

        #region Create

        [Test]
        public void Create_ValidRole_OKResult()
        {
            const string EXPECTED_RESPONSE = "Должность \"TestRole\" создана";

            var rolesQuantity = _manage.GetAll().Count;

            var role = new Role
            {
                RoleId = 200,
                Name = "TestRole",
                SeeFlightPrice = true,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
                CanManageRole = true,
                Employees = new List<Employee>()
            };

            _response = _controller.Create(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedRolesQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _roleRepository.GetAll().Contains(role);

            Assert.That(_response, Is.InstanceOf<OkObjectResult>());
            Assert.That(isPresentInRepository, Is.True);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedRolesQuantity, rolesQuantity + 1);
        }

        [Test]
        public void Create_RoleWithEmptyName_BadResult()
        {
            const string EXPECTED_RESPONSE = "Не передано название должности!";
            var rolesQuantity = _manage.GetAll().Count;

            var role = new Role
            {
                RoleId = 400,
                Name = "",
                SeeFlightPrice = true,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
                CanManageRole = true,
                Employees = new List<Employee>()
            };

            _response = _controller.Create(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedRolesQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _roleRepository.GetAll().Contains(role);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedRolesQuantity, rolesQuantity);
        }

        [Test]
        public void Create_ExistingRoleWithThatName_BadResult()
        {
            const string EXPECTED_RESPONSE = "Должность с таким названием уже существует!";
            var rolesQuantity = _manage.GetAll().Count;

            var role = new Role
            {
                RoleId = 400,
                Name = "Директор",
                SeeFlightPrice = true,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
                CanManageRole = true,
                Employees = new List<Employee>()
            };

            _response = _controller.Create(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedRolesQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _roleRepository.GetAll().Contains(role);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedRolesQuantity, rolesQuantity);
        }

        [Test]
        public void Create_RoleIsNull_BadResult()
        {
            const string EXPECTED_RESPONSE = "Должность не передана!";
            var rolesQuantity = _manage.GetAll().Count;

            Role role = null;

            _response = _controller.Create(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedRolesQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _roleRepository.GetAll().Contains(role);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedRolesQuantity, rolesQuantity);
        }

        [Test]
        public void Create_Role_CantManageRole_BadResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на создание должности!";
            var rolesQuantity = _manage.GetAll().Count;

            Role role = null;

            _response = _controller.Create(role, ROLE_ID_THAT_CANT_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedRolesQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _roleRepository.GetAll().Contains(role);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedRolesQuantity, rolesQuantity);
        }

        #endregion

        #region Delete

        [Test]
        public void Delete_RoleId_200_OKResult()
        {
            const int ROLE_ID = 200;
            var roleQuantity = _manage.GetAll().Count;
            var foundRole = _manage.GetById(ROLE_ID);

            _response = _controller.Delete(ROLE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var isPresentInRepository = _manage.GetAll().Contains(foundRole);
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(currentEmployeeQuantity, roleQuantity - 1);
        }

        [Test]
        public void Delete_RoleId_404_NotFound()
        {
            const int ROLE_ID = 404;
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";

            _response = _controller.Delete(ROLE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void Delete_RoleId_200_RoleIdThatCantManageRole_BadRequestResult()
        {
            const int ROLE_ID = 200;
            const string EXPECTED_RESPONSE = "Нет прав на удаление должности!";
            var roleQuantity = _manage.GetAll().Count;
            var foundRole = _manage.GetById(ROLE_ID);

            _response = _controller.Delete(ROLE_ID, ROLE_ID_THAT_CANT_MANAGE_ROLE);
            var isPresentInRepository = _manage.GetAll().Contains(foundRole);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.True);
            Assert.AreEqual(roleQuantity, currentEmployeeQuantity);
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        #endregion

        #region RemoveEmployee

        [Test]
        public void RemoveRoleEmployee_EmployeeId200_RoleId200_OKResult()
        {
            var employeeRepository = new TestEmployeeRepository();
            var roleRepository = new TestRoleRepository();
            var manage = new RoleManage(roleRepository, employeeRepository);
            var controller = new RolesController(manage);
            const int EMPLOYEE_ID = 200;
            const int ROLE_ID = 200;
            var foundRole = _manage.GetById(ROLE_ID);
            var foundEmployee = _manage.GetEmployee(EMPLOYEE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" больше не занимает должность \"{foundRole.Name}\"";

            var response = controller.RemoveRoleEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (OkObjectResult)response;
            var responseResultValue = responseResult.Value;
            var employeePresentInRoleEmployees = foundRole.Employees.Any(e => e.EmployeeId == foundEmployee.EmployeeId);

            Assert.That(response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(employeePresentInRoleEmployees, Is.False);
        }

        [Test]
        public void RemoveRoleEmployee_EmployeeId200_RoleId200_RoleIdThatCantManageRole_OKResult()
        {
            var employeeRepository = new TestEmployeeRepository();
            var roleRepository = new TestRoleRepository();
            var manage = new RoleManage(roleRepository, employeeRepository);
            var controller = new RolesController(manage);
            const int EMPLOYEE_ID = 200;
            const int ROLE_ID = 200;
            var foundRole = _manage.GetById(ROLE_ID);
            var foundEmployee = _manage.GetEmployee(EMPLOYEE_ID);
            var expectedResponse = "Нет прав на удаление сотрудника у должности!";

            var response = controller.RemoveRoleEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CANT_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)response;
            var responseResultValue = responseResult.Value;
            var employeePresentInRoleEmployees = foundRole.Employees.Any(e => e.EmployeeId == foundEmployee.EmployeeId);

            Assert.That(response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(employeePresentInRoleEmployees, Is.False);
        }

        [Test]
        public void RemoveRoleEmployee_ValidEmployeeId_ValidRoleId_BadRequestObjectResult()
        {
            const int EMPLOYEE_ID = 200;
            const int ROLE_ID = 2;
            var foundRole = _manage.GetById(ROLE_ID);
            var foundEmployee = _manage.GetEmployee(EMPLOYEE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" не занимает должность \"{foundRole.Name}\"";

            var response = _controller.RemoveRoleEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)response;
            var responseResultValue = responseResult.Value;
            var employeePresentInRoleEmployees = foundRole.Employees.Any(e => e.EmployeeId == foundEmployee.EmployeeId);

            Assert.That(response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(employeePresentInRoleEmployees, Is.False);
        }

        [Test]
        public void RemoveRoleEmployee_NonExistentEmployeeId_ValidRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";
            const int EMPLOYEE_ID = 404;
            const int ROLE_ID = 1;

            _response = _controller.RemoveRoleEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void RemoveRoleEmployee_ValidEmployeeId_NonExistentRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";
            const int EMPLOYEE_ID = 1;
            const int ROLE_ID = 404;

            _response = _controller.RemoveRoleEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        #endregion

        #region Update

        [Test]
        public void Update_ValidRole_OKResult()
        {
            var role = new Role
            {
                RoleId = 1,
                Name = "Старший менеджер",
                SeeFlightPrice = false,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
            };

            _response = _controller.Update(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var foundRole = _manage.GetById(role.RoleId);

            var expectedCollectionJson = JsonConvert.SerializeObject(role);
            var actualCollectionJson = JsonConvert.SerializeObject(foundRole);

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.That(expectedCollectionJson, Is.EquivalentTo(actualCollectionJson));
        }

        [Test]
        public void Update_ValidRole_RoleIdThatCantManageRole_BadResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на изменение данных должности!";
            var role = new Role
            {
                RoleId = 1,
                Name = "Генеральный директор",
                SeeFlightPrice = false,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
            };

            _response = _controller.Update(role, ROLE_ID_THAT_CANT_MANAGE_ROLE);
            var foundEmployee = _manage.GetById(role.RoleId);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
            Assert.That(foundEmployee.Name, Is.Not.EqualTo(role.Name));
        }

        [Test]
        public void Update_InvalidRole_BadRequest()
        {
            const string EXPECTED_RESPONSE = "Не передано название должности!";
            var role = new Role
            {
                RoleId = 1,
                Name = "",
                SeeFlightPrice = false,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
            };

            _response = _controller.Update(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var foundEmployee = _manage.GetById(role.RoleId);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
            Assert.That(foundEmployee.Name, Is.Not.EqualTo(role.Name));
        }

        [Test]
        public void Update_RoleId404_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";
            var role = new Role
            {
                RoleId = 404,
                Name = "Менеджер",
                SeeFlightPrice = false,
                SeeFlightAirCraft = true,
                SeeFlightCustomer = true,
                SeeFlightQuantityPax = true,
                SeeFlightDirections = true,
                SeeFlightComment = true,
                SeeCrew = true,
            };
            var foundEmployee = _manage.GetById(role.RoleId);

            _response = _controller.Update(role, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
            Assert.That(foundEmployee, Is.Null);
        }

        #endregion

        #region SetEmployee

        [Test]
        public void SetEmployee_ValidRoleId_ValidEmployeeId_OKResult()
        {
            const int ROLE_ID = 2;
            const int EMPLOYEE_ID = 2;
            var foundRole = _manage.GetById(EMPLOYEE_ID);
            var foundEmployee = _manage.GetEmployee(ROLE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" теперь занимает должность \"{foundRole.Name}\"";

            _response = _controller.SetEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(foundRole.Employees.Contains(foundEmployee));
        }

        [Test]
        public void SetEmployee_ValidEmployeeId_RoleIdThatCantManageRole_BadRequestResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на установление сотрудникам должности!";
            const int EMPLOYEE_ID = 1;
            const int ROLE_ID = 404;

            _response = _controller.SetEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CANT_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void SetEmployee_NonExistentEmployeeId_ValidRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";
            const int EMPLOYEE_ID = 404;
            const int ROLE_ID = 1;

            _response = _controller.SetEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void SetEmployee_ValidEmployeeId_NonExistentRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";
            const int EMPLOYEE_ID = 1;
            const int ROLE_ID = 404;

            _response = _controller.SetEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void SetEmployee_ValidEmployeeId_ValidRoleId_BadRequestObjectResult()
        {
            const int ROLE_ID = 1;
            const int EMPLOYEE_ID = 1;
            var foundRole = _manage.GetById(EMPLOYEE_ID);
            var foundEmployee = _manage.GetEmployee(ROLE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" уже занимает должность \"{foundRole.Name}\"";

            _response = _controller.SetEmployee(ROLE_ID, EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_ROLE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
        }

        #endregion
    }
}