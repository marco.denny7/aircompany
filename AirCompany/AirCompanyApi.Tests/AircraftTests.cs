using System.Linq;
using AirCompanyApi.Controllers;
using AirCompanyApi.Management;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AirCompanyApi.Tests
{
    public class AircraftTests
    {
        private IAircraftRepository _aircraftRepository;
        private IRoleRepository _roleRepository;
        private IAircraftManage _manage;
        private AircraftsController _controller;
        private IActionResult _response;
        private const int ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT = 2;
        private const int ROLE_ID_THAT_CANT_MANAGE_AIRCRAFT = 1;

        [SetUp]
        public void Setup()
        {
            _aircraftRepository = new TestAircraftRepository();
            _roleRepository = new TestRoleRepository();
            _manage = new AircraftManage(_aircraftRepository, _roleRepository);
            _controller = new AircraftsController(_manage);
        }

        [TearDown]
        public void TearDown()
        {
            _aircraftRepository = null;
            _manage = null;
            _controller = null;
            _response = null;
        }

        #region Get

        [Test]
        public void GetAll_ShouldReturn_OKResult()
        {
            _response = _controller.GetAll(ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var expectedResult = _aircraftRepository.GetAll();

            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(expectedResult, responseResultValue);
        }

        [Test]
        public void GetAll_RoleWithoutRights_BadRequestResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по самолётам";
            _response = _controller.GetAll(ROLE_ID_THAT_CANT_MANAGE_AIRCRAFT);

            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void GetAircraft_Id_1_OKResult()
        {
            const int AIRCRAFT_ID = 1;
            var expectedAircraft = _aircraftRepository.GetById(AIRCRAFT_ID);

            _response = _controller.GetAircraft(AIRCRAFT_ID, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(expectedAircraft, responseResultValue);
        }

        [Test]
        public void GetAircraft_Id_404_NotFoundResult()
        {
            const int AIRCRAFT_ID = 404;
            const string EXPECTED_RESPONSE = "Самолёт с таким id не найден!";

            _response = _controller.GetAircraft(AIRCRAFT_ID, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void GetAircraft_AircraftId_404_RoleId_1_BadObjectResult()
        {
            const int AIRCRAFT_ID = 1;
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по самолёту";

            _response = _controller.GetAircraft(AIRCRAFT_ID, ROLE_ID_THAT_CANT_MANAGE_AIRCRAFT);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        #endregion

        #region Create

        [Test]
        public void Create_ValidAircraft_OKResult()
        {
            var aircraftsQuantity = _manage.GetAll().Count;
            var aircraft = new Aircraft()
            {
                AircraftId = 200,
                Name = "Enbraer 200",
                BoardNumber = "Em-007"
            };
            string EXPECTED_RESPONSE = $"Самолёт \"{aircraft.Name} с бортовым номером {aircraft.BoardNumber}\" создан";

            _response = _controller.Create(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedEmployeeQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _aircraftRepository.GetAll().Contains(aircraft);

            Assert.That(_response, Is.InstanceOf<OkObjectResult>());
            Assert.That(isPresentInRepository, Is.True);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedEmployeeQuantity, aircraftsQuantity + 1);
        }

        [Test]
        public void Create_ExistingAircraft_BadRequestResult()
        {
            var aircraftsQuantity = _manage.GetAll().Count;
            var aircraft = new Aircraft
            {
                AircraftId = 200,
                Name = "Embraer 200",
                BoardNumber = "Em-009"
            };
            string EXPECTED_RESPONSE = $"Самолёт с бортовым номером \"{aircraft.BoardNumber}\" уже есть!";

            _response = _controller.Create(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedEmployeeQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _aircraftRepository.GetAll().Contains(aircraft);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedEmployeeQuantity, aircraftsQuantity);
        }

        [Test]
        public void Create_AircraftWithoutName_BadRequestResult()
        {
            var aircraftsQuantity = _manage.GetAll().Count;
            var aircraft = new Aircraft
            {
                AircraftId = 200,
                Name = "",
                BoardNumber = "Em-008"
            };
            string EXPECTED_RESPONSE = "Не передано название самолёта!";

            _response = _controller.Create(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedEmployeeQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _aircraftRepository.GetAll().Contains(aircraft);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedEmployeeQuantity, aircraftsQuantity);
        }

        [Test]
        public void Create_AircraftWithoutBoardNumber_BadRequestResult()
        {
            var aircraftsQuantity = _manage.GetAll().Count;
            var aircraft = new Aircraft
            {
                AircraftId = 200,
                Name = "Embraer 190",
                BoardNumber = ""
            };
            const string EXPECTED_RESPONSE = "Не передан бортовой номер самолёта!";

            _response = _controller.Create(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedEmployeeQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _aircraftRepository.GetAll().Contains(aircraft);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedEmployeeQuantity, aircraftsQuantity);
        }

        [Test]
        public void Create_ValidAircraft_RoleWithAdministrations_OKResult()
        {
            const int ROLE_ID = 1;
            const string EXPECTED_RESPONSE = "Нет прав на создание самолёта";
            var aircraftsQuantity = _manage.GetAll().Count;
            var aircraft = new Aircraft
            {
                AircraftId = 200,
                Name = "Enbraer 200",
                BoardNumber = "Em-007"
            };

            _response = _controller.Create(aircraft, ROLE_ID_THAT_CANT_MANAGE_AIRCRAFT);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedEmployeeQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _aircraftRepository.GetAll().Contains(aircraft);

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedEmployeeQuantity, aircraftsQuantity);
        }

        #endregion

        #region Delete

        [Test]
        public void Delete_RoleWithAdministrationRights_2_AircraftId_4_OKResult()
        {
            const int ROLE_ID = 2;
            const int AIRCRAFT_ID = 4;
            var aircraftsQuantity = _manage.GetAll().Count;
            var foundAircraft = _manage.GetById(AIRCRAFT_ID);

            _response = _controller.Delete(AIRCRAFT_ID, ROLE_ID);
            var isPresentInRepository = _manage.GetAll().Contains(foundAircraft);
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(currentEmployeeQuantity, aircraftsQuantity - 1);
        }

        [Test]
        public void Delete_RoleWithoutAdministrationRights_1_AircraftId_4_BadRequestResult()
        {
            const int ROLE_ID = 1;
            const int AIRCRAFT_ID = 4;
            const string EXPECTED_RESPONSE = "Нет прав на удаление самолёта";
            var aircraftsQuantity = _manage.GetAll().Count;
            var foundAircraft = _manage.GetById(AIRCRAFT_ID);

            _response = _controller.Delete(AIRCRAFT_ID, ROLE_ID);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var isPresentInRepository = _manage.GetAll().Contains(foundAircraft);
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.True);
            Assert.That(currentEmployeeQuantity, Is.Not.EqualTo(aircraftsQuantity - 1));
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void Delete_RoleWithAdministrationRights_2_AircraftId_404_NotFoundResult()
        {
            const int ROLE_ID = 2;
            const int AIRCRAFT_ID = 404;
            const string EXPECTED_RESPONSE = "Самолёт с таким id не найден!";
            var aircraftsQuantity = _manage.GetAll().Count;

            _response = _controller.Delete(AIRCRAFT_ID, ROLE_ID);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.That(currentEmployeeQuantity, Is.Not.EqualTo(aircraftsQuantity - 1));
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        #endregion

        #region Update

        [Test]
        public void Update_ValidAircraft_RoleWithAircraftAdminRights_2_OKResult()
        {
            var aircraft = new Aircraft
            {
                AircraftId = 4,
                Name = "Test Name",
                BoardNumber = "Test Board number",
                IsFree = false
            };

            _response = _controller.Update(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var foundAircraft = _manage.GetById(aircraft.AircraftId);

            var expectedAircraftJson = JsonConvert.SerializeObject(aircraft);
            var actualAircraftJson = JsonConvert.SerializeObject(foundAircraft);

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.That(expectedAircraftJson, Is.EquivalentTo(actualAircraftJson));
            Assert.AreEqual(expectedAircraftJson, actualAircraftJson);
        }

        [Test]
        public void Update_ValidAircraft_RoleWithoutAircraftAdminRights_1_BadRequestResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на обновление данных самолёта";
            var aircraft = new Aircraft
            {
                AircraftId = 2,
                Name = "Test Name",
                BoardNumber = "Test Board number",
                IsFree = false
            };

            _response = _controller.Update(aircraft, ROLE_ID_THAT_CANT_MANAGE_AIRCRAFT);
            var foundAircraft = _manage.GetById(aircraft.AircraftId);
            var expectedAircraftJson = JsonConvert.SerializeObject(aircraft);
            var actualAircraftJson = JsonConvert.SerializeObject(foundAircraft);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(expectedAircraftJson, Is.Not.EquivalentTo(actualAircraftJson));
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void Update_RoleWithAdministrationRights_2_AircraftId_404_NotFoundResult()
        {
            const int AIRCRAFT_ID = 404;
            const string EXPECTED_RESPONSE = "Самолёт с таким id не найден!";
            var aircraftsQuantity = _manage.GetAll().Count;

            _response = _controller.Delete(AIRCRAFT_ID, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.That(currentEmployeeQuantity, Is.Not.EqualTo(aircraftsQuantity - 1));
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void Update_AircraftWithoutName_RoleWithAircraftAdminRights_2_BadRequestResult()
        {
            const string EXPECTED_RESPONSE = "Не передано название самолёта!";
            var aircraft = new Aircraft
            {
                AircraftId = 4,
                Name = " ",
                BoardNumber = "Test Board number",
                IsFree = false
            };

            _response = _controller.Update(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var foundAircraft = _manage.GetById(aircraft.AircraftId);
            var expectedAircraftJson = JsonConvert.SerializeObject(aircraft);
            var actualAircraftJson = JsonConvert.SerializeObject(foundAircraft);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(expectedAircraftJson, Is.Not.EquivalentTo(actualAircraftJson));
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void Update_AircraftWithSameBoardNumber_RoleWithAircraftAdminRights_2_BadRequestResult()
        {
            var aircraft = new Aircraft
            {
                AircraftId = 4,
                Name = "Test Name",
                BoardNumber = "M-Moon",
                IsFree = false
            };
            var aircraftWithSameBoardNumber =
                _aircraftRepository.GetAll().FirstOrDefault(a => a.BoardNumber == aircraft.BoardNumber);
            var expectedResponse =
                $"Самолёт с бортовым номером \"{aircraftWithSameBoardNumber.BoardNumber}\" уже есть!";

            _response = _controller.Update(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var foundAircraft = _manage.GetById(aircraft.AircraftId);
            var expectedAircraftJson = JsonConvert.SerializeObject(aircraft);
            var actualAircraftJson = JsonConvert.SerializeObject(foundAircraft);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(expectedAircraftJson, Is.Not.EquivalentTo(actualAircraftJson));
            Assert.AreEqual(expectedResponse, responseResultValue);
        }

        [Test]
        public void Update_AircraftWithoutBorderNumb_RoleWithAircraftAdminRights_2_BadRequestResult()
        {
            const string EXPECTED_RESPONSE = "Не передан бортовой номер самолёта!";
            var aircraft = new Aircraft
            {
                AircraftId = 4,
                Name = "Test Name",
                BoardNumber = " ",
                IsFree = false
            };

            _response = _controller.Update(aircraft, ROLE_ID_THAT_CAN_MANAGE_AIRCRAFT);
            var foundAircraft = _manage.GetById(aircraft.AircraftId);
            var expectedAircraftJson = JsonConvert.SerializeObject(aircraft);
            var actualAircraftJson = JsonConvert.SerializeObject(foundAircraft);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(expectedAircraftJson, Is.Not.EquivalentTo(actualAircraftJson));
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        #endregion
    }
}