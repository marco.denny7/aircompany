using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Tests
{
    public class TestEmployeeRepository : IEmployeeRepository
    {
        private List<Employee> Employees { get; }

        public TestEmployeeRepository()
        {
            var testData = new TestDataWithEmployees();
            Employees = testData.Employees;
        }

        public Employee GetById(int id)
        {
            return Employees.FirstOrDefault(e => e.EmployeeId == id);
        }

        public List<Employee> GetAll()
        {
            return Employees;
        }

        public void Delete(Employee employee)
        {
            Employees.Remove(employee);
        }

        public void Update(Employee employee)
        {
            var foundEmployee = GetById(employee.EmployeeId);
            if (string.IsNullOrEmpty(employee.Name))
            {
                foundEmployee.Name = employee.Name;
            }

            if (string.IsNullOrEmpty(employee.Surname))
            {
                foundEmployee.Surname = employee.Surname;
            }

            if (string.IsNullOrEmpty(employee.Email))
            {
                foundEmployee.Email = employee.Email;
            }

            if (string.IsNullOrEmpty(employee.Password))
            {
                foundEmployee.Password = employee.Password;
            }

            foundEmployee.Roles = employee.Roles;
        }

        public void Add(Employee employee)
        {
            Employees.Add(employee);
        }

        public void Save()
        {
        }

        public void RemoveEmployeeRole(Employee employee, Role role)
        {
            employee.Roles.RemoveAll(r => r.RoleId == role.RoleId);
        }
    }
}