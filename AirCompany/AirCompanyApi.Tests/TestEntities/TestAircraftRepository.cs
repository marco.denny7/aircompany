using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Tests
{
    public class TestAircraftRepository : IAircraftRepository
    {
        private List<Aircraft> _aircrafts;

        public TestAircraftRepository()
        {
            var testData = new TestDataWithAircrafts();
            _aircrafts = testData.Aircrafts;
        }

        public Aircraft GetById(int id)
        {
            return _aircrafts.FirstOrDefault(a => a.AircraftId == id);
        }

        public List<Aircraft> GetAll()
        {
            return _aircrafts;
        }

        public void Delete(Aircraft aircraft)
        {
            _aircrafts.Remove(aircraft);
        }

        public void Update(Aircraft aircraft)
        {
            var foundAircraft = GetById(aircraft.AircraftId);

            foundAircraft.Name = aircraft.Name;
            foundAircraft.BoardNumber = aircraft.BoardNumber;
            foundAircraft.IsFree = aircraft.IsFree;
        }

        public void Add(Aircraft aircraft)
        {
            _aircrafts.Add(aircraft);
        }

        public void Save()
        {
        }
    }
}