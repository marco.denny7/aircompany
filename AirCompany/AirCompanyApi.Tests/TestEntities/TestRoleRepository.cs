using System.Collections.Generic;
using System.Linq;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;

namespace AirCompanyApi.Tests
{
    public class TestRoleRepository : IRoleRepository
    {
        private List<Role> Roles { get; }

        public TestRoleRepository()
        {
            var testRolesData = new TestDataWithRoles();
            Roles = testRolesData.Roles;
        }

        public Role GetById(int id)
        {
            return Roles.FirstOrDefault(r => r.RoleId == id);
        }

        public List<Role> GetAll()
        {
            return Roles;
        }

        public void Delete(Role role)
        {
            Roles.Remove(role);
        }

        public void Update(Role role)
        {
            var foundRole = GetById(role.RoleId);

            if (foundRole.Name is not (null and " "))
            {
                foundRole.Name = role.Name;
            }

            foundRole.Name = role.Name;
            foundRole.SeeFlightCustomer = role.SeeFlightCustomer;
            foundRole.SeeFlightAirCraft = role.SeeFlightAirCraft;
            foundRole.SeeFlightPrice = role.SeeFlightPrice;
            foundRole.SeeFlightQuantityPax = role.SeeFlightQuantityPax;
            foundRole.SeeFlightDirections = role.SeeFlightDirections;
            foundRole.SeeFlightComment = role.SeeFlightComment;
            foundRole.SeeCrew = role.SeeCrew;
            foundRole.Employees = role.Employees;
            foundRole.Employees = role.Employees;
        }

        public void Add(Role role)
        {
            Roles.Add(role);
        }

        public void Save()
        {
        }

        public void RemoveRoleEmployee(Role role, Employee employee)
        {
            role.Employees.RemoveAll(e => e.EmployeeId == employee.EmployeeId);
        }
    }
}