using System.Linq;
using AirCompanyApi.Controllers;
using AirCompanyApi.Management;
using AirCompanyApi.Management.Interfaces;
using AirCompanyApi.Models;
using AirCompanyApi.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;

namespace AirCompanyApi.Tests
{
    public class EmployeeTests
    {
        private IEmployeeRepository _employeeRepository;
        private IRoleRepository _roleRepository;
        private IEmployeeManage _manage;
        private EmployeesController _controller;
        private IActionResult _response;
        private const int ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE = 2;
        private const int ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE = 1;

        [SetUp]
        public void Setup()
        {
            _employeeRepository = new TestEmployeeRepository();
            _roleRepository = new TestRoleRepository();
            _manage = new EmployeeManage(_employeeRepository, _roleRepository);
            _controller = new EmployeesController(_manage);
        }

        [TearDown]
        public void TearDown()
        {
            _employeeRepository = null;
            _roleRepository = null;
            _manage = null;
            _controller = null;
            _response = null;
        }

        #region Get

        [Test]
        public void GetAll_ShouldReturn_OKResult()
        {
            _response = _controller.GetAll(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE);
            var expectedResult = _employeeRepository.GetAll();

            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(expectedResult, responseResultValue);
        }

        [Test]
        public void GetAll_RoleIdThatCantManageEmployee_BadResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по сотрудникам!";

            _response = _controller.GetAll(ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void GetEmployee_Id_1_OKResult()
        {
            const int EMPLOYEE_ID = 1;
            var expectedEmployee = _employeeRepository.GetById(EMPLOYEE_ID);

            _response = _controller.GetEmployee(EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(expectedEmployee, responseResultValue);
        }

        [Test]
        public void GetEmployee_Id_1__NotFoundResult()
        {
            const int EMPLOYEE_ID = 1;
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по сотруднику!";

            _response = _controller.GetEmployee(EMPLOYEE_ID, ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void GetEmployee_Id_404_NotFoundResult()
        {
            const int EMPLOYEE_ID = 404;
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";

            _response = _controller.GetEmployee(EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        #endregion

        #region GetEmployeeRoles

        [Test]
        public void GetEmployeeRoles_EmployeeId_2_OKResult()
        {
            const int EMPLOYEE_ID = 200;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            _response = _controller.GetEmployeeRoles(EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE);

            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(foundEmployee.Roles, responseResultValue);
        }

        [Test]
        public void GetEmployeeRoles_EmployeeId_404_RoleIdThatCantManageEmployee_BadResult()
        {
            const int EMPLOYEE_ID = 200;
            const string EXPECTED_RESPONSE = "Нет прав на получение информации по сотруднику!";
            _response = _controller.GetEmployeeRoles(EMPLOYEE_ID, ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE);

            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void GetEmployeeRoles_EmployeeId_1_NotFoundResult()
        {
            const int EMPLOYEE_ID = 2;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            var expectedResponse =
                $"{foundEmployee.Surname} {foundEmployee.Name} не занимает должностей!";
            _response = _controller.GetEmployeeRoles(EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE);

            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(expectedResponse, responseResultValue);
        }

        [Test]
        public void GetEmployeeRoles_EmployeeId_404_NotFoundResult()
        {
            const int EMPLOYEE_ID = 404;
            const string expectedResponse = "Сотрудник с таким id не найден!";
            _response = _controller.GetEmployeeRoles(EMPLOYEE_ID, ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE);

            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(expectedResponse, responseResultValue);
        }

        #endregion

        #region Create

        [Test]
        public void Create_ValidEmployee_OKResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник \"Doe John\" добавлен";
            var employeeQuantity = _manage.GetAll().Count;
            var employee = new Employee
            {
                EmployeeId = 200,
                Name = "John",
                Surname = "Doe",
                Email = "john@gmail.com",
                Password = "123456"
            };

            _response = _controller.Create(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, employee);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var expectedEmployeeQuantity = _manage.GetAll().Count;
            var isPresentInRepository = _employeeRepository.GetAll().Contains(employee);

            Assert.That(_response, Is.InstanceOf<OkObjectResult>());
            Assert.That(isPresentInRepository, Is.True);
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
            Assert.AreEqual(expectedEmployeeQuantity, employeeQuantity + 1);
        }

        [Test]
        public void Create_EmployeeEmptyName_BadRequestObjectResult()
        {
            const string EXPECTED_RESPONSE = "Не передано имя сотрудника!";
            var employee = new Employee
            {
                EmployeeId = 200,
                Name = "",
                Surname = "Doe",
                Email = "john@gmail.com",
                Password = "123456"
            };

            _response = _controller.Create(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, employee);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var isPresentInRepository = _employeeRepository.GetAll().Contains(employee);

            Assert.That(isPresentInRepository, Is.False);
            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void Create_EmployeeEmptySurname_BadRequestObjectResult()
        {
            const string EXPECTED_RESPONSE = "Не передана фамилия сотрудника!";
            var employee = new Employee
            {
                EmployeeId = 200,
                Name = "John",
                Surname = "",
                Email = "john@gmail.com",
                Password = "123456"
            };

            _response = _controller.Create(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, employee);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var isPresentInRepository = _employeeRepository.GetAll().Contains(employee);

            Assert.That(isPresentInRepository, Is.False);
            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void Create_ExistingEmployee_BadRequestObjectResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник с таким Email уже существует!";
            var employee = new Employee
            {
                EmployeeId = 404,
                Name = "Василий",
                Surname = "Пупкин",
                Password = "Pupok99",
                Email = "Pupkin@gmail.com"
            };

            _response = _controller.Create(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, employee);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void Create_EmployeeEmptyEmail_BadRequestObjectResult()
        {
            const string EXPECTED_RESPONSE = "Не передан Email сотрудника!";
            var employee = new Employee
            {
                EmployeeId = 200,
                Name = "John",
                Surname = "Doe",
                Email = "",
                Password = "123456"
            };

            _response = _controller.Create(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, employee);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var isPresentInRepository = _employeeRepository.GetAll().Contains(employee);

            Assert.That(isPresentInRepository, Is.False);
            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void Create_EmployeeEmptyPassword_BadRequestObjectResult()
        {
            const string EXPECTED_RESPONSE = "Не передан пароль сотрудника!";
            var employee = new Employee
            {
                EmployeeId = 200,
                Name = "John",
                Surname = "Doe",
                Email = "john@gmail.com",
                Password = ""
            };

            _response = _controller.Create(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, employee);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;
            var isPresentInRepository = _employeeRepository.GetAll().Contains(employee);

            Assert.That(isPresentInRepository, Is.False);
            Assert.That(_response, Is.InstanceOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        #endregion

        #region Delete

        [Test]
        public void Delete_EmployeeId_200_OKResult()
        {
            const int EMPLOYEE_ID = 200;
            var employeesQuantity = _manage.GetAll().Count;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);

            _response = _controller.Delete(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID);
            var isPresentInRepository = _manage.GetAll().Contains(foundEmployee);
            var currentEmployeeQuantity = _manage.GetAll().Count;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.That(isPresentInRepository, Is.False);
            Assert.AreEqual(currentEmployeeQuantity, employeesQuantity - 1);
        }
        
        [Test]
        public void Delete_EmployeeId_200_RoleIdThatCantManageEmployee_BadResult()
        {
            const int EMPLOYEE_ID = 200;
            const string EXPECTED_RESPONSE = "Нет прав на удаление сотрудника!";
            var employeesQuantity = _manage.GetAll().Count;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);

            _response = _controller.Delete(ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE, EMPLOYEE_ID);
            var isPresentInRepository = _manage.GetAll().Contains(foundEmployee);
            var currentEmployeeQuantity = _manage.GetAll().Count;
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(isPresentInRepository, Is.True);
            Assert.AreEqual(currentEmployeeQuantity, employeesQuantity);
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void Delete_EmployeeId_404_NotFound()
        {
            const int EMPLOYEE_ID = 404;
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";

            _response = _controller.Delete(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        #endregion

        #region Update

        [Test]
        public void Update_ValidEmployee_OKResult()
        {
            var employee = new Employee
            {
                EmployeeId = 1,
                Name = "Иван",
                Surname = "Ургант",
                Password = "Urgant99",
                Email = "Urgant@gmail.com"
            };
            var foundEmployee = _manage.GetById(employee.EmployeeId);

            _response = _controller.Update(employee);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value as Employee;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(foundEmployee.EmployeeId, responseResultValue.EmployeeId);
            Assert.AreEqual(foundEmployee.Name, responseResultValue.Name);
            Assert.AreEqual(foundEmployee.Surname, responseResultValue.Surname);
            Assert.AreEqual(foundEmployee.Password, responseResultValue.Password);
            Assert.AreEqual(foundEmployee.Email, responseResultValue.Email);
        }

        [Test]
        public void Update_EmployeeId404_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";
            var employee = new Employee
            {
                EmployeeId = 404,
                Name = "Иван",
                Surname = "Ургант",
                Password = "Urgant99",
                Email = "Urgant@gmail.com"
            };
            var foundEmployee = _manage.GetById(employee.EmployeeId);

            _response = _controller.Update(employee);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
            Assert.That(foundEmployee, Is.Null);
        }

        #endregion

        #region SetRole

        [Test]
        public void SetRole_ValidEmployeeId_ValidRoleId_OKResult()
        {
            const int EMPLOYEE_ID = 2;
            const int ROLE_ID = 2;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            var foundRole = _manage.GetRole(ROLE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" теперь занимает должность \"{foundRole.Name}\"";

            _response = _controller.SetRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (OkObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(foundEmployee.Roles.Contains(foundRole));
        }
        
        [Test]
        public void SetRole_ValidEmployeeId_ValidRoleId_RoleIdThatXantManageEmployee_BadResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на добавление должности сотруднику!";
            const int EMPLOYEE_ID = 2;
            const int ROLE_ID = 1;

            _response = _controller.SetRole(ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (BadRequestObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void SetRole_NonExistentEmployeeId_ValidRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";
            const int EMPLOYEE_ID = 404;
            const int ROLE_ID = 1;

            _response = _controller.SetRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void SetRole_ValidEmployeeId_NonExistentRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";
            const int EMPLOYEE_ID = 1;
            const int ROLE_ID = 404;

            _response = _controller.SetRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void SetRole_EmployeeId1_RoleId1_BadRequestObjectResult()
        {
            var employeeRepository = new TestEmployeeRepository();
            var roleRepository = new TestRoleRepository();
            var manage = new EmployeeManage(employeeRepository, roleRepository);
            var controller = new EmployeesController(manage);

            const int EMPLOYEE_ID = 1;
            const int ROLE_ID = 1;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            var foundRole = _manage.GetRole(ROLE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" уже занимает должность \"{foundRole.Name}\"";

            var response = controller.SetRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (BadRequestObjectResult)response;
            var responseResultValue = responseResult.Value;

            Assert.That(response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
        }

        #endregion

        #region RemoveEmployeeRole

        [Test]
        public void RemoveEmployeeRole_ValidEmployeeId_ValidRoleId_OKResult()
        {
            var employeeRepository = new TestEmployeeRepository();
            var roleRepository = new TestRoleRepository();
            var manage = new EmployeeManage(employeeRepository, roleRepository);
            var controller = new EmployeesController(manage);

            const int EMPLOYEE_ID = 200;
            const int ROLE_ID = 200;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            var foundRole = _manage.GetRole(ROLE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" больше не занимает должность \"{foundRole.Name}\"";

            var response = controller.RemoveEmployeeRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (OkObjectResult)response;
            var responseResultValue = responseResult.Value;
            var rolePresentInEmployeeRoles = foundEmployee.Roles.Any(r => r.RoleId == foundRole.RoleId);

            Assert.That(response, Is.TypeOf<OkObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(rolePresentInEmployeeRoles, Is.False);
        }
        
        [Test]
        public void RemoveEmployeeRole_ValidEmployeeId_RoleIdThatCantManageEmployee_ValidRoleId_OKResult()
        {
            const string EXPECTED_RESPONSE = "Нет прав на удаление должности у сотрудника!";
            var employeeRepository = new TestEmployeeRepository();
            var roleRepository = new TestRoleRepository();
            var manage = new EmployeeManage(employeeRepository, roleRepository);
            var controller = new EmployeesController(manage);

            const int EMPLOYEE_ID = 200;
            const int ROLE_ID = 200;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            var foundRole = _manage.GetRole(ROLE_ID);

            var response = controller.RemoveEmployeeRole(ROLE_ID_THAT_CANT_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (BadRequestObjectResult)response;
            var responseResultValue = responseResult.Value;
            var rolePresentInEmployeeRoles = foundEmployee.Roles.Any(r => r.RoleId == foundRole.RoleId);

            Assert.That(response, Is.TypeOf<BadRequestObjectResult>());
            Assert.That(rolePresentInEmployeeRoles, Is.True);
            Assert.AreEqual(EXPECTED_RESPONSE, responseResultValue);
        }

        [Test]
        public void RemoveEmployeeRole_ValidEmployeeId_ValidRoleId_BadRequestObjectResult()
        {
            const int EMPLOYEE_ID = 200;
            const int ROLE_ID = 2;
            var foundEmployee = _manage.GetById(EMPLOYEE_ID);
            var foundRole = _manage.GetRole(ROLE_ID);
            var expectedResponse =
                $"Сотрудник: \"{foundEmployee.Name} {foundEmployee.Surname}\" не занимает должность \"{foundRole.Name}\"";

            var response = _controller.RemoveEmployeeRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE,EMPLOYEE_ID, ROLE_ID);
            var responseResult = (BadRequestObjectResult)response;
            var responseResultValue = responseResult.Value;
            var rolePresentInEmployeeRoles = foundEmployee.Roles.Any(r => r.RoleId == foundRole.RoleId);

            Assert.That(response, Is.TypeOf<BadRequestObjectResult>());
            Assert.AreEqual(responseResultValue, expectedResponse);
            Assert.That(rolePresentInEmployeeRoles, Is.False);
        }

        [Test]
        public void RemoveEmployeeRole_NonExistentEmployeeId_ValidRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Сотрудник с таким id не найден!";
            const int EMPLOYEE_ID = 404;
            const int ROLE_ID = 1;

            _response = _controller.RemoveEmployeeRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        [Test]
        public void RemoveEmployeeRole_ValidEmployeeId_NonExistentRoleId_NotFoundResult()
        {
            const string EXPECTED_RESPONSE = "Должность с таким id не найдена!";
            const int EMPLOYEE_ID = 1;
            const int ROLE_ID = 404;

            _response = _controller.RemoveEmployeeRole(ROLE_ID_THAT_CAN_MANAGE_EMPLOYEE, EMPLOYEE_ID, ROLE_ID);
            var responseResult = (NotFoundObjectResult)_response;
            var responseResultValue = responseResult.Value;

            Assert.That(_response, Is.TypeOf<NotFoundObjectResult>());
            Assert.AreEqual(responseResultValue, EXPECTED_RESPONSE);
        }

        #endregion
    }
}